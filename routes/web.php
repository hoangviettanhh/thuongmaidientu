<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('login');
//});

Route::get('/',[\App\Http\Controllers\auth\AuthController::class,'index']);
Route::get('/loguot',[\App\Http\Controllers\auth\AuthController::class,'loguot'])->name('loguot');
Route::post('/login',[\App\Http\Controllers\auth\AuthController::class,'login']);
Route::post('/signin',[\App\Http\Controllers\auth\AuthController::class,'signin']);


Route::get('/home',[\App\Http\Controllers\home\HomeController::class,'index'])->name('home')->middleware('checkUserLogin');


Route::get('/create',[\App\Http\Controllers\home\CreateProductController::class,'index'])->name('index');
Route::post('/getproduct',[\App\Http\Controllers\home\CreateProductController::class,'getproduct'])->name('getproduct');
Route::post('/addcreate',[\App\Http\Controllers\home\CreateProductController::class,'addCreate'])->name('addCreate');
Route::get('/edit/{id}',[\App\Http\Controllers\home\CreateProductController::class,'edit'])->name('edit');
Route::post('/updateproduct',[\App\Http\Controllers\home\CreateProductController::class,'updateproduct'])->name('updateproduct');
Route::get('/delete/{id}',[\App\Http\Controllers\home\CreateProductController::class,'delete'])->name('delete');



Route::get('/out', function () {
    if(session()->has('login')){
        session()->pull('login');
    }
    return redirect('/');
});
